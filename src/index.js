'use strict'

// js
import './js/main'

// css
import '../node_modules/swiper/swiper-bundle.min.css'
import './assets/css/main.css'

// scss
import './assets/scss/main.scss'
