'use strict'

// Swiper
import Swiper, { Navigation } from 'swiper';


(function () {
  const html = document.querySelector('html');


  // Добавляет слайдер в мобильную версию
  const swiper = new Swiper('.readiness', {
    slidesPerView: 1,
    spaceBetween: 20,
    loop: true,
    // autoHeight: true,

    // Навигация
    modules: [ Navigation ],
    navigation: {
      prevEl: '.readiness-prev',
      nextEl: '.readiness-next'
    },

    breakpoints: {
      768: {
        // setWrapperSize: true,
        spaceBetween: 28,
        slidesPerView: 3,
        loop: false
      },
      1248: {
        spaceBetween: 55,
        slidesPerView: 3,
        loop: false
      }
    }
  });


  // Добавляет параллакс эффект
  let stars_1 = document.querySelector('.stars_1'),
      stars_2 = document.querySelector('.stars_2');

  if (stars_1) {
    window.addEventListener('scroll', () => {
      let { scrollY } = window;

      stars_1.style.top = (38 + -.25 * scrollY) + 'px';
      stars_2.style.top = (139 + .25 * scrollY) + 'px';
    });
  }


  // Добавляет всплывающие блоки
  let objectWrappers = document.querySelectorAll('.object-wrapper');

  objectWrappers.forEach(objectWrapper => {
    let objects = objectWrapper.querySelectorAll('.object');

    objects.forEach(object => {
      object.classList.remove('object-animation');

      let observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
          if (typeof getCurrentAnimationPreference === 'function' && !getCurrentAnimationPreference()) {
            return;
          }

          if (entry.isIntersecting) {
            object.classList.add('object-animation');
            return;
          }

          object.classList.remove('object-animation');
        });
      });

      observer.observe(objectWrapper);
    });
  });


  // Добавляет раскрывающийся список FAQ
  const allQuestions = document.querySelectorAll('.faq-list__question');

  function toggleQuestions() {
    let questionToggle = this.getAttribute('area-expanded'),
        icon = this.querySelector('.faq-list__icon svg use');

    for (let i = 0; i < allQuestions.length; i++) {
      allQuestions[i].setAttribute('area-expanded', 'false');
      allQuestions[i].querySelector('.faq-list__icon svg use').setAttribute('href', '/image/icon/plus.svg#plus');
    }

    if (questionToggle == 'false') {
      this.setAttribute('area-expanded', 'true');
      icon.setAttribute('href', '/image/icon/minus.svg#minus');
    }
  }

  allQuestions.forEach(question => question.addEventListener('click', toggleQuestions));


  // Открывает меню
  let linkOpenMenu = document.querySelectorAll('[data-action=open-menu]');

  function openMenu(event) {
    event.preventDefault();

    let menu = document.querySelector('.menu');

    // Плавная прокрутка к началу страницы, чтобы выровнять меню
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });

    menu.classList.add('menu--open');
    html.style.overflowY = 'hidden';

    event.stopPropagation();
  }

  linkOpenMenu.forEach(link => link.addEventListener('click', openMenu));


  // Закрывает меню
  let linkCloseMenu = document.querySelectorAll('[data-action=close-menu]');

  function closeMenu(event) {
    event.preventDefault();

    let menu = document.querySelector('.menu');

    menu.classList.remove('menu--open');
    html.style.overflowY = 'visible';

    event.stopPropagation();
  }

  linkCloseMenu.forEach(link => link.addEventListener('click', closeMenu));


  // Открывает popup-окно
  const linkOpenPopup = document.querySelectorAll('[data-action=open-popup]');

  function openPopup(event, module) {
    event.preventDefault();

    let popup = document.querySelector('.popup');

    if (module === undefined) {
      module = this.getAttribute('data-popup');
    }

    popup.classList.add('popup--open');
    document.querySelector('.' + module).classList.add('popup__module--open');
    html.style.overflowY = 'hidden';
  }

  linkOpenPopup.forEach(link => link.addEventListener('click', openPopup));


  // Закрывает popup-окно
  const linkClosePopup = document.querySelectorAll('[data-action=close-popup]');

  function closePopup(event) {
    event.preventDefault();

    let popup = document.querySelector('.popup'),
        modules = document.querySelectorAll('.popup__module');

    modules.forEach(module => {
      if (module.classList.contains('popup__module--open')) {
        module.classList.remove('popup__module--open');
      }
    });
    popup.classList.remove('popup--open');
    html.style.overflowY = 'visible';
  }

  linkClosePopup.forEach(link => link.addEventListener('click', closePopup));


  // Обрабатывает отправку форм
  const buttons = document.querySelectorAll('[data-action=send-form]');

  function submitForm(event) {
    event.preventDefault();

    let form = this.closest('form'),
        formName = form.getAttribute('name');

    // Добавляет disabled к кнопке, при попытке отправить пустую форму
    checkInput(form);

    if (this.classList.contains('disabled')) {
      // Подсвечивает незаполненные поля
      form.querySelectorAll('.empty').forEach(selector => selector.classList.add('error'));

      setTimeout(() => {
        form.querySelectorAll('.empty').forEach(selector => selector.classList.remove('error'));
      }, 1500);
    }

    else {
      // Отправляет форму
      const request = new XMLHttpRequest();

      let name = form.querySelector('input[name=name]'),
          email = form.querySelector('input[name=email]'),
          comment = form.querySelector('textarea[name=comment]'),
          url = '/index.php?name=' + name + '&email=' + email + '&comment=' + comment; // Адрес файла принимающего данные формы

      request.open('GET', url);
      request.setRequestHeader('Content-Type', 'application/x-www-form-url');
      request.addEventListener('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
          console.log(request.responseText);
        }
      });

      // Очищает заполненные поля формы
      form.querySelectorAll('input, textarea').forEach(selector => {
        selector.value = '';
      });

      // Открывает попап "Спасибо"
      if (formName == 'request') {
        closePopup(event);
      }
      openPopup(event, 'thanks');
    }
  }

  // Проверяет заполнение обязательных полей формы и блокирует кнопку
  function checkInput(form) {
    let button = form.querySelector('[data-action=send-form]');

    form.querySelectorAll('input, textarea').forEach(selector => {
      if (selector.hasAttribute('required')) {
        if (selector.value.length < 1) {
          selector.classList.add('empty');
        } else {
          selector.classList.remove('empty');
        }
      }
    });

    let countEmpty = form.querySelectorAll('.empty').length;

    if (countEmpty > 0) {
      if (!button.classList.contains('disabled')) {
        button.classList.add('disabled');
      }
    } else {
      button.classList.remove('disabled');
    }
  }

  buttons.forEach(button => button.addEventListener('click', submitForm));
})();
