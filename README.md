# Школа китайского языка Say Nihao

## Установка зависимостей проекта
npm install

## Режим разработки
npm run dev

Проект будет доступен по адресу:
http://localhost:8081/

## Тестирование
npm run test

## Режим сборки
npm run build
